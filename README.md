CREATE USERS
=========

Создание\удаление пользователей на серверах ОС linux

Requirements
------------


Role Variables
--------------

Используется переменная CREATE_USERS_USERS которая включает в себя список с массивами пользователя.
Пример:
```
CREATE_USERS_USERS
  - {
      "user": "user1",                              - имя пользователя 
      "pass": "pass1",                              - пароль пользователя в открытом виде 
      "shell": "/bin/bash",                         - командный интерфейс
      "groups": "",                                 - группы в которые нужно добавить
      "remove": "false"                             - указываем действие false - создание пользователя, true - удаление    
    }
  - {"user": "user2", "pass": "pass2", "shell": "/bin/bash", "groups": "", "remove": "false"}
 ```

Dependencies
------------



Example Playbook
----------------

```
---
- name: Test new role from within this playbook
  hosts: local
  gather_facts: true
  become: true
  variables:
    CREATE_USERS_USERS:
      - {"user": "user1", "pass": "pass1", "shell": "/bin/bash", "groups": "", "remove": "false"}
      - {"user": "user2", "pass": "pass2", "shell": "/bin/bash", "groups": "", "remove": "false"}
  roles:
    - /home/stilet/project/ansible/roles/create_users
```

License
-------

BSD

Author Information
------------------


